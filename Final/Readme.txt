﻿TỔNG KẾT

Cả nhóm sơ bộ đã hoàn thành công việc chung của nhóm, cụ thể :


- Nguyễn Trần Đức Tài : UseCase Trả xe

 + Vẽ các biểu đồ trình tự cho các lớp trong use case trả xe
 + Vẽ màn hình homescreen bao gồm danh sách bãi xe cho user , thông tin xe đang thuê cho user ,vẽ màn hình giao dịch trả xe, màn hình danh sách bãi xe đang có để trả, màn hình thông tin các user đang thuê xe của admin
 + Vẽ các biểu đồ trình tự và thiết kế lớp chi tiết cho Trả xe  rentbikecontroller paymentcontroller va homecontroller
 + Tham gia dựng code base
 + Code các màn : BikeRenting, ReturnBike, Payment, RentBikeTransactionInfo, UserRentingBike 
 + Viết unit test cho : RentBikeController, PaymentController
 + Phần trăm đóng góp vào công việc chung : 25%


- Nguyễn Đức Thành : UseCase Thuê xe

 + Vẽ các biểu đồ trình tự cho các lớp trong use case thuê xe
 + Vẽ màn hình danh sách xe cho user, màn hình thông tin chi tiết xe cho user,vẽ màn hình giao dịch thuê xe, vẽ màn hình thông tin thanh toán,,vẽ màn hình đăng nhập
 + Vẽ các biểu đồ trình tự và thiết kế lớp chi tiết cho Thuê xe rentbikecontroller , paymentcontroller và logincontroller
 + Tham gia dựng code base
 + Code các màn : Home, Login, BikeDetail, RentBike, Payment
 + Viết unit test cho : RentBikeController, PaymentController
 + Phần trăm đóng góp vào công việc chung : 25%


- Nguyễn Hoàng Anh : UseCase Thêm/Sửa xe

 + Vẽ các biểu đồ trình tự cho các lớp trong use case thêm /sửa xe
 + Vẽ form cập nhật thông tin xe, vẽ màn hình kết quả,Thêm xe: Vẽ màn hình danh sách xe bao gồm edit và cập nhật cho admin
 + Vẽ các biểu đồ trình tự và thiết kế lớp chi tiết cho Cập nhật thông tin xe , Thêm xe và bikecontroller
 + Code các màn : updateBike, addBike
 + Viết unit test cho : BikeController
 + Phần trăm đóng góp vào công việc chung : 20%

- Phạm Tuấn Hiên : UseCase Thêm/Sửa bãi xe

 + Vẽ các biểu đồ trình tự cho các lớp trong use case thêm /sửa bãi xe
 + Cập nhật thông tin bãi xe: Vẽ màn hình thông tin chi tiết bao gồm edit và cập nhật bãi xe cho admin ( trong màn hình homescreen), vẽ màn hình thêm bãi xe
 + Vẽ các biểu đồ trình tự và thiết kế lớp chi tiết cho Cập nhật thông tin bãi xe , thêm bãi xe và stationcontroller 
 + Code các màn : updateStation, addStation
 + Viết unit test cho : StationController
 + Phần trăm đóng góp vào công việc chung : 20%

- Quốc Đạt : Usecase thêm xe 

 + Chỉ làm bài tập tuần đến homework 04 và không làm bài tập lớn nên usecase thêm xe được giao lại cho Nguyễn Hoàng Anh
 + Phần trăm đóng góp vào công việc chung : 5%

- Ta Lattanavong : Usecase thêm bãi xe

 + Chỉ làm bài tập tuần đến homework 04 và không làm bài tập lớn nên usecase thêm bãi xe được giao lại cho Phạm Tuấn Hiên
 + Phần trăm đóng góp vào công việc chung : 5%