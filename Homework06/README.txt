Công việc của nhóm HW06

Nguyễn Trần Đức Tài
 + Tham gia dựng code base
 + Code các màn : BikeRenting, ReturnBike, Payment, UserRentingBike 
 + Viết unit test cho : RentBikeController, PaymentController

Nguyễn Đức Thành 
 + Tham gia dựng code base
 + Code các màn : Home, Login, BikeDetail, RentBike, Payment
 + Viết unit test cho : RentBikeController, PaymentController

Nguyễn Hoàng Anh
 + Code các màn : updateBike, addBike
 + Viết unit test cho : BikeController

Phạm Tuấn Hiên
 + Code các màn : updateStation, addStation
 + Viết unit test cho : StationController