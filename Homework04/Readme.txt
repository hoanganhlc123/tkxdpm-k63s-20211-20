Bai tap ve nha tuan 4
Nhiệm vụ cả nhóm : Vẽ sơ đồ chuyển đổi màn hình(screen transition diagram) và thiết kế giao hiện màn hình chính. 

Nhiệm vụ cá nhân : Đặc tả các màn hình trong use case của mỗi người. Thiết kế subsystem Interbank
- Nguyễn Hoàng Anh 20180010: Cập nhật thông tin xe:vẽ form cập nhật thông tin xe, vẽ màn hình kết quả,Thêm xe: Vẽ màn hình danh sách xe bao gồm edit và cập nhật cho admin 
- Nguyễn Đức Thành 20183991: Thuê xe: Vẽ màn hình danh sách xe cho user, màn hình thông tin chi tiết xe cho user,vẽ màn hình giao dịch thuê xe, vẽ màn hình thông tin thanh toán,,vẽ màn hình đăng nhập
- Nguyễn Trần Đức Tài 20180168: Trả xe: Vẽ màn hình homescreen bao gồm danh sách bãi xe cho user , thông tin xe đang thuê cho user ,vẽ màn hình giao dịch trả xe, màn hình danh sách bãi xe đang có để trả, màn hình thông tin các user đang thuê xe của admin 
- Phạm Tuấn Hiên 20183527: Cập nhật thông tin bãi xe: Vẽ màn hình thông tin chi tiết bao gồm edit và cập nhật bãi xe cho admin ( trong màn hình homescreen), vẽ màn hình thêm bãi xe



Mô tả sơ đồ màn hình chung : Trong mainscreen sẽ bao gồm phần đăng nhập, đăng xuất cho admin/user, mục thông tin xe đang thuê cho màn hình user và danh sách bãi xe. Xét màn hình user : khi ấn vào 1 bãi xe bất kì sẽ hiện lên màn hình danh sách xe, ấn vào 1 xe sẽ hiện lên màn hình thông tin chi tiết xe và nút button Thuê xe bên cạnh, ấn vào nút thuê xe sẽ hiện lên màn hình giao dịch thuê xe và nút xác nhận thuê. Khi ấn xác nhận thuê sẽ hiện lên màn hình thanh toán cho user. Sau khi thành công , mình quay lại xem mục thông tin xe đang thuê , trong này sẽ có button trả xe , sau khi ấn sẽ có màn hình danh sách bãi xe để chọn nơi trả, sau khi chọn được nơi trả sẽ hiện màn hình giao dịch trả xe và nút thanh toán, ấn nút thanh toán sẽ hiện màn hình thanh toán và khi thanh toán thành công sẽ thông báo trả xe thành công. Xét màn hình admin : cũng tương tự như màn hình user nhưng các phần xe, bãi xe sẽ có phần edit thông tin và thêm xe/bãi xe và không có button thuê xe, mục thông tin xe đang thuê cho admin sẽ hiện thông tin những ai đang thuê xe nào và ở bãi nào, khi ấn vào từng mục sẽ hiện thông tin chi tiết xe đang thuê giống trong phần user. 
