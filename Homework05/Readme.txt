Bai tap ve nha tuan 5
Nhiệm vụ cả nhóm : Gộp lại các biểu đồ lớp thiết kế của mỗi thành viên, tổ chức thành các package
cho hợp lý, thống nhất cách thức đặt tên.
Sau đó, vẽ biểu đồ package cho toàn nhóm và cho từng cá nhân (cần phân thành các tầng)

Nhiệm vụ cá nhân : Vẽ các biểu đồ trình tự cho các lớp thiết kế trong use case mình phụ trách. Có
thể cần vẽ nhiều biểu đồ, mỗi biểu đồ ứng với một scenario trong use case. Sau đó tìm các hành
vi và thuộc tính cho các lớp thiết kế và vẽ biểu đồ lớp thiết kế cho use case mình phụ trách ( lớp controller, entity, DAO, DAOImpl, Handler )

- Nguyễn Hoàng Anh 20180010: Cập nhật thông tin xe , Thêm xe và bikecontroller

- Nguyễn Đức Thành 20183991: Thuê xe rentbikecontroller , paymentcontroller và logincontroller

- Nguyễn Trần Đức Tài 20180168: Trả xe  rentbikecontroller paymentcontroller va homecontroller

- Phạm Tuấn Hiên 20183527: Cập nhật thông tin bãi xe , thêm bãi xe và stationcontroller


