package application;

import javafx.fxml.FXML;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

public class CalculatorController  implements Initializable {
	 	@FXML
	   private Button addBtn;
	 
	   @FXML
	   private TextField arg1;
	   
	   @FXML
	   private TextField arg2;
	   
	   @FXML
	   private Label label;
	 
	   @Override
	   public void initialize(URL location, ResourceBundle resources) {

	       // TODO (don't really need to do anything here).
	       // TODO (Thực sự cũng không cần thiết viết gì ở đây).
	     
	   }

	   // When user click on myButton
	   // this method will be called.
	   // Khi người dùng nhấn vào Button addBtn
	   // phương thức này sẽ được gọi
	   public void addTwoNumber(ActionEvent event) {
//	       System.out.println("Button Clicked!");
	 
//	       System.out.println("arg1 " +arg1.getText());
	       
	       	int a = Integer.parseInt(arg1.getText());
	       	int b = Integer.parseInt(arg2.getText());
	       
	       	
	       	label.setText(String.valueOf(a+b));
	     
	   }
	 
}
