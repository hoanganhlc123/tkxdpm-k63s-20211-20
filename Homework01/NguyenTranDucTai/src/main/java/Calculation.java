import java.util.Scanner;

public class Calculation {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter two numbers: ");
        double num1 = sc.nextDouble();
        double num2 = sc.nextDouble();
        double sum = num1 + num2;
        double difference = num1 - num2;
        double product  = num1*num2;
        double quotient = num1/num2;
        System.out.println("The addition of two numbers is : " + sum);
        System.out.println("The subtraction of two numbers is : " + difference);
        System.out.println("The multiplication of two numbers is : " + product);
        System.out.println("The division of two numbers is : " + quotient);

    }
}
